#!/bin/sh

# We run "nproc" based number of workers for the API
python3 -m gunicorn \
--name benjamin-mathias-api \
--bind=0.0.0.0:"${PORT:-80}" \
--access-logfile '-' \
--log-file '-' \
--workers "$(nproc)" \
-k uvicorn.workers.UvicornWorker app.main:app
