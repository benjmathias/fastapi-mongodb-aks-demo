import unittest

import requests

URL = "https://datas.to/api/v1/status"


class TestStatusRoute(unittest.TestCase):
    def test_status_route(self):
        expected_response = {
            "success": True,
            "message": "everything's fine, the sun is blue 🏖️",
            "name": "benjamin-mathias-demo",
            "status": "ok",
            "version": "x.x.x",
            "components": "",
        }
        res = requests.get(URL)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json(), expected_response)
