from fastapi import FastAPI

from .helpers.errors.override_default import override_default
from .helpers.services.mongodb_connect import close_mongo_connection, connect_to_mongo
from .routers import payload_route, status_route, image_route

app = FastAPI()
override_default(app)
v1 = "/api/v1"

app.include_router(status_route.router, prefix=v1)
app.include_router(payload_route.router, prefix=v1)
app.include_router(image_route.router, prefix=v1)

app.add_event_handler("startup", connect_to_mongo)
app.add_event_handler("shutdown", close_mongo_connection)
